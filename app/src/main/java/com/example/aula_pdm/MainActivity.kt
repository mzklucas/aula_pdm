package com.example.aula_pdm

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.OutputStreamWriter
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val botao_clique = findViewById<Button>(R.id.button)
        val botao_dados =  findViewById<Button>(R.id.button2)
        val texto = findViewById<TextView>(R.id.textView)
        val background = findViewById<ConstraintLayout>(R.id.back)
        var tempo = System.currentTimeMillis()
        background.setBackgroundColor(getColor(R.color.white))


        botao_clique.setOnClickListener {
            texto.setText ("Clicado")

            val newParam:ConstraintLayout.LayoutParams = (botao_clique.layoutParams as ConstraintLayout.LayoutParams)
            newParam.verticalBias = Random.nextFloat ()
            newParam.horizontalBias = Random.nextFloat ()
            botao_clique.layoutParams = newParam

            texto.setText("Tempo de clique " + ((System.currentTimeMillis() - tempo).toString()))

            val stream_output: FileOutputStream = openFileOutput("mytextfile.txt", Context.MODE_PRIVATE)
            val outputWriter = OutputStreamWriter(stream_output)
            outputWriter.write((System.currentTimeMillis() - tempo).toString())
            outputWriter.close()
            stream_output.close()

        }

        botao_dados.setOnClickListener {

            val stream_input: FileInputStream = openFileInput("mytextfile.txt")
            val saida = stream_input.readBytes().toString(Charsets.UTF_8)
            texto.setText (saida)
            stream_input.close()
        }


    }
}